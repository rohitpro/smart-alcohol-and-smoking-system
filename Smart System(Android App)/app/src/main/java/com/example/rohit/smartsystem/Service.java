package com.example.rohit.smartsystem;

import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by rohit on 29/10/17.
 */

public class Service extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String token= FirebaseInstanceId.getInstance().getToken();
        Toast.makeText(getApplicationContext(),token,Toast.LENGTH_LONG).show();
    }
}
