package com.example.rohit.smartsystem;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.ubidots.ApiClient;
import com.ubidots.Variable;

public class AdminActivity extends AppCompatActivity {

    public String alcohol_Data="Alcohol_data";
    public String SmokeData="SmokeData";
    TextView smoke;
    TextView alcohol;
    public BroadcastReceiver broadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int dataalco=intent.getIntExtra(alcohol_Data,0);
            int datasmoke=intent.getIntExtra(SmokeData,0);
            alcohol.setText(Integer.toString(dataalco)+"%");
            smoke.setText(Integer.toString(datasmoke)+"%");
            new ApiUbidots().execute(dataalco);
            new ApiUbidots().execute(datasmoke);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        smoke= (TextView) findViewById(R.id.smoke);
        alcohol= (TextView) findViewById(R.id.alcohol);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(broadcastReceiver,new IntentFilter(Intent.ACTION_SCREEN_ON));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(broadcastReceiver);
    }

    public class ApiUbidots extends AsyncTask<Integer,Void,Void>
    {
        public final String API_KEY="A1E-b0030f9945e6a531a5181b61afc6c748ab60";
        public final String Var_Id ="59a6533ec03f97517802cd9d";
        public final String Var_ID="59ff4667c03f977605992e37";

        @Override
        protected Void doInBackground(Integer... integers) {
            ApiClient apiClient=new ApiClient(API_KEY);
            Variable smokeValue=apiClient.getVariable(Var_Id);
            Variable alcohoValue=apiClient.getVariable(Var_ID);
            smokeValue.saveValue(integers[0]);
            alcohoValue.saveValue(integers[1]);
            return null;
        }
    }
}
