package com.example.rohit.smartsystem;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    Button signin,signup;
    GoogleApiClient mGoogleApiclient;
    int RC_CODE=1;
    GoogleSignInOptions gso;
    SignInButton signInButton;
    FirebaseAuth mAuth;
    EditText email,password;
    TextInputLayout username,pass;
    FirebaseUser user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        signin= (Button) findViewById(R.id.signin);
        signup= (Button) findViewById(R.id.signup);
        Service s= new Service();
        OneSignal.startInit(this).setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
            @Override
            public void notificationReceived(OSNotification osNotification) {
                JSONObject data = osNotification.payload.additionalData;
                String customKey;

                if (data != null) {
                    customKey = data.optString("customkey", null);
                    if (customKey != null)
                        Log.i("OneSignalExample", "customkey set with value: " + customKey);
                }
            }
        }).setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
            @Override
            public void notificationOpened(OSNotificationOpenResult osNotificationOpenResult) {

                JSONObject data=osNotificationOpenResult.notification.payload.additionalData;
                String customToken=data.optString("customToken",null);
                Toast.makeText(getApplicationContext(),customToken,Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(),MainActivity.class));

            }
        }).autoPromptLocation(false).init();
        MessagingService m= new MessagingService();
        signInButton= (SignInButton) findViewById(R.id.signIn);
        email= (EditText) findViewById(R.id.email);
        password= (EditText) findViewById(R.id.password);
        username= (TextInputLayout) findViewById(R.id.username);
        pass= (TextInputLayout) findViewById(R.id.pass);
        mAuth=FirebaseAuth.getInstance();
        user=mAuth.getCurrentUser();
        gso=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail().build();

        mGoogleApiclient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,null)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signInWithGoogle();
            }
        });

    }


    public void signIn(final View view)
    {
        String Email,Password;
        Email=email.getText().toString().trim();
        Password=password.getText().toString().trim();
        if(Email.isEmpty()||Password.isEmpty()) {
            if (Email.isEmpty()) {
                username.setError("Email is required");
            }
            if (Password.isEmpty()) {
                pass.setError("Password is required");
            }
        }
        else {
            mAuth.signInWithEmailAndPassword(Email, Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    if (task.isSuccessful()) {

                        boolean verifyEmail = user.isEmailVerified();
                        if (verifyEmail) {
                            startActivity(new Intent(getApplicationContext(), AdminActivity.class));
                        } else {
                            Snackbar.make(view, "Verify Your Email", Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(view, "Authentication Failure", Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        }
        }

    public void signUpAdmin(View view)
    {
        startActivity(new Intent(this,SignUp.class));
    }
    public void signInWithGoogle()
    {
        Intent intent= Auth.GoogleSignInApi.getSignInIntent(mGoogleApiclient);
        startActivityForResult(intent,RC_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==RC_CODE)
        {
            GoogleSignInResult result=Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(result.isSuccess())
            {
                GoogleSignInAccount account=result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {

        AuthCredential credential= GoogleAuthProvider.getCredential(account.getIdToken(),null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful())
                {
                    startActivity(new Intent(getApplicationContext(),AdminActivity.class));
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
