package com.example.rohit.smartsystem;

import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class SignUp extends AppCompatActivity {

    TextInputLayout Emailerror,Passerror,Repasserror;
    Button signUp;
    EditText email,password,repass;
    FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        email= (EditText) findViewById(R.id.email);
        password= (EditText) findViewById(R.id.pass);
        signUp= (Button) findViewById(R.id.signUp);
        Emailerror= (TextInputLayout) findViewById(R.id.Email);
        Passerror= (TextInputLayout) findViewById(R.id.Pass);
        Repasserror= (TextInputLayout) findViewById(R.id.Repass);
        repass= (EditText) findViewById(R.id.repass);
        mAuth=FirebaseAuth.getInstance();
    }

    public void signUpUser(final View view) {
        String Email, Password, Repass;
        Email = email.getText().toString().trim();
        Password = password.getText().toString().trim();
        Repass = repass.getText().toString().trim();
        if (Email.isEmpty() || Password.isEmpty()||Repass.isEmpty()) {
            if (Email.isEmpty()) {
                Emailerror.setError("Email is required");
            }
            if (Password.isEmpty()) {
                Passerror.setError("Password is required");
            }
            if (Repass.isEmpty()) {
                Repasserror.setError("Please re-enter your password");
            }


        }
        else if(!(Password.equalsIgnoreCase(Repass)))
        {
            Snackbar.make(view,"Password doesn't match",Snackbar.LENGTH_LONG).show();
        }

        else {

            mAuth.createUserWithEmailAndPassword(Email, Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {


                    if (task.isSuccessful()) {
                        FirebaseUser user = mAuth.getCurrentUser();
                        Toast.makeText(getApplicationContext(), "Registered", Toast.LENGTH_LONG).show();
                        user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                Toast.makeText(getApplicationContext(), "Verification link has been sent to your gmail Account.", Toast.LENGTH_LONG).show();
                            }
                        });

                     //   finish();

                    } else {

                        Snackbar.make(view, "Authentication Failure", Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
